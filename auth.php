<?php
require_once('UberAPI.php');

$uberAPI = new UberAPI(
  $_SERVER['CLIENT_ID'],
  $_SERVER['CLIENT_SECRET'],
  $_SERVER['SERVER_TOKEN']
);

$validScopes = [
  'profile',
  'places',
  'ride_widgets',
  'history_lite',
  'history',
  'request_receipt',
  'request',
  'all_trips',
];

if ($scopes = $_GET['scopes']) {
  $authUrl = UberAPI::$baseAuthURL.'/oauth/v2/authorize';
  $qs = http_build_query([
    'response_type' => 'code',
    'client_id' => $_SERVER['CLIENT_ID'],
    'scope' => $scopes,
  ]);
  header("Location: $authUrl?$qs");
  die();
} else if ($code = $_GET['code']) {
  $results = $uberAPI->getUserToken($code);
  foreach ($results as $key => $value) {
    echo "<b>$key:</b> <i>$value</i><br>";
  }
} else {
  echo authForm(implode(' ', $validScopes));
}

function authForm($scopes) {
return
'
<html>
<body>
  <form action="auth">
    Scopes:&nbsp;
    <input type="text" name="scopes" value="'.$scopes.'" size="'.strlen($scopes).'"><br>
    <br>
    <input type="submit" name="Auth"<br> 
  </form>
</body>
</html>
';
}
