<?php

class UberAPI {
  public static $baseAuthURL = 'https://login.uber.com';

  private static $baseURL = 'https://api.uber.com';
  private static $tokenRefreshOffset = 600;

  private $clientId;
  private $clientSecret;
  private $serverToken;
  private $userToken;

  public function __construct($clientId, $clientSecret, $serverToken) {
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->serverToken = $serverToken;
  }

  public function get($url) {
    if (substr($url, -1) != '?') {
      $url .= '&';
    }
    $url .= "server_token=$this->serverToken";
    return self::http('GET', $url);
  }

  public function post($url, $headers, $data) {
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    return self::http('POST', $url, $headers, $data);
  }

  public function getUserToken($code) {
    $results = $this->post(
      UberAPI::$baseAuthURL.'/oauth/v2/token',
      [],
      http_build_query([
        'client_secret' => $this->clientSecret,
        'client_id' => $this->clientId,
        'grant_type' => 'authorization_code',
        'redirect_uri' => $_SERVER['AUTH_URI'],
        'code' => $code,
      ])
    );
    $this->userToken = $results;
    $this->userToken['client_time'] = time();
    return $this->userToken;
  }

  public function refreshUserToken() {
     
  }

  public static function url($endpoint, $params=[]) {
    return self::$baseURL."/$endpoint?".http_build_query($params);
  }

  public static function http($method, $url, $headers=[], $data=null) {
    $method = strtoupper($method);
    $opts = [];
    if ($method == 'POST') {
      $opts = [
        'http' => [
          'method' => $method,
          'header' => implode("\r\n", $headers),
          'content' => $data,
        ],
      ];
    }
    $context = stream_context_create($opts);
echo "$url\n";
    $results = file_get_contents($url, false, $context);
    $json = json_decode($results, true);
    return $json;
  }
}
